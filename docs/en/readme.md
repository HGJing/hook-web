# Timer Hooker
![Views](https://palerock.cn/node-service/images/greasyfork/views-info/@hook-js_timer_hook.timer.user?name=Views)
![Daily Installs](https://palerock.cn/node-service/images/scripts/install/@hook-js_timer_hook.timer.user?label=Daily%20Installs&daily=1)
![Daily Updates](https://palerock.cn/node-service/images/scripts/update/@hook-js_timer_hook.timer.user?label=Daily%20Updates&daily=1)
![Total Installs](https://palerock.cn/node-service/images/scripts/install/@hook-js_timer_hook.timer.user?label=Total%20Installs)
![Score](https://palerock.cn/node-service/images/greasyfork/info/fan_score/372673?name=Score&rcolor=orange)
![Good Ratings](https://palerock.cn/node-service/images/greasyfork/info/good_ratings/372673?name=Good%20Ratings&rcolor=darkcyan)
[![Latest Version](https://palerock.cn/node-service/images/badge.svg?name=Latest%20Version&status=2.0.12&rcolor=%235E70CC)](https://palerock.cn/node-service/scripts/install/@hook-js_timer/hook.timer.user.js)

**Timer Hooker** is a script based on *Tampermonkey*, used for the acceleration/deceleration of the timer of any webpage, and the fast/slow playback of the video player.

## Installation / Usage

1. Install the matching *Tampermonkey* extension for any browser.
2. Click the [Install Link](https://palerock.cn/node-service/scripts/install/@hook-js_timer/hook.timer.user.js) to install.
3. After the installation is complete, enter any website, there is a floating energy ball on the left side of the page, and there is an acceleration option when you move the mouse. You can choose to accelerate or decelerate, and the floating ball on the top indicates the current double speed.
4. Shortcut key operation：
    - `’ctrl‘+‘=’` or `’ctrl‘+'>'` : Increase the magnification (default +2)
    - `’alt‘+‘=’` or `’alt‘+‘>’` : The magnification is increased by multiples (the default is multiplied by 2)
    - `’ctrl‘+‘-’` or `’ctrl‘+‘<’` : Reduce the magnification (default -2)
    - `’alt‘+‘-’` or `’ctrl‘+‘<’` : The magnification is reduced by multiples (default divided by 2)
    - `’ctrl‘+‘0’` or `’alt‘+‘0’` : The magnification is restored to 1
    - `’ctrl‘+‘9’` or `’alt‘+‘9’` : Pop up a custom magnification input box
5. **The script supports custom settings, select from the right-click menu of the mouse `Tampermonkey > TimerHooker > Open Configuration Page`，or directly open the configuration link: [https://timer.palerock.cn/configuration/](https://timer.palerock.cn/configuration/)**

## Why *Timer Hooker*
Regarding the purpose of the script, the script is used as a timer to control the webpage, and the script can be used according to the following purposes.
1. Skip the annoying advertisement countdown
   
     Many video sites will have advertisements before playing the video. It usually takes at least one or two minutes to watch the advertisement. The plug-in can speed up the countdown of this advertisement, and even skip the advertisement in one second.

2. Break through the upper/lower limit of video playback speed and unlock the playback rate limit
   
     But also for a lot of video to the site, the default speedup in `0.5 - 2.0` between that is twice as slow or speed doubled, and even some video sites also not allowed to change the play rate, then the script can be used to unlock the playback rate limit, from `0.065` to `16` switch freely between fold increase in the efficiency of playing video site.

3. Eliminate useless waiting time
   
     Similar to video countdown advertisements, many websites will set a waiting time similar to the membership threshold in some places. If it is not a member who needs to wait for a period of time, then you can use this plugin to speed up the waiting time.

4. More uses waiting to be explored
   
     Since this script is a timing function that acts on the browser fundamentally, you can use this script wherever you need to change the timing efficiency. You are welcome to explore and share them one by one.


## Precautions
1. About the fast / slow playback of the video
   
   - Only supports `HTML5` video player, is not supported by `flash player`.
   - Once the script is used to fast/slow the video, the playback speed of the video itself or other scripts will be invalid. If you want to use other scripts to control the video speed, please set the script speed to the default value: `1`.
  
2. Regarding some websites, the page display time is skipped after the acceleration is turned on, but the countdown is reset after the page is refreshed
   
   - It may be that the background of the website is involved in time calculations, and the script can only work on the browser, so it is invalid.
   - It is possible that the countdown is done locally, but an unknown countdown technique is used. You can report the situation and contact the author.

3. The script is effective for all webpages by default. If you want to only take effect for certain websites or exclude certain websites, please refer to the following article for details:

    [How to configure the Tampermonkey plug-in to enable the plug-in to take effect only on specific web pages？](https://palerock.cn/articles/001w1s6gHGV?lang=en)

## Script Modularity
From the `v2.0.0` starting version of the script from the bottom of the substantial reconstruction, the various functional modular, currently has the following several built-in module.
1. `ConfigModule` (The core configuration module is used to realize the unification of global configuration)
2. `DefinitionModule` (The core definition module is used to implement the underlying Hook)
3. `ShadowDomModule` (Core module, used for retrieval ShadowDOM)
4. `TimerModule` (Pre-installed module, used to hijack the browser timer function)
5. `DateModule` (Pre-assembled modules for hijacking Datefunction)
6. `VideoSpeedModule` (Pre-installed module, used to hijack H5 video playback rate)
7. `ShortcutModule` (Pre-installed module, used to set shortcut keys, expandable)
8. `UiModule` (Pre-installed module for basic UI display, replaceable)
   
### Advanced Module
The script will provide a variety of new functional modules, such as new UI modules, specific modules that take effect on certain websites. These modules will be available in the script store and can be purchased for a fee.

### Custom Module
Contact the author, you can get other custom modules for a fee.

## Feedback
- All kinds of feedback or suggestions are welcome.
- If the plug-in does not take effect on any website, it must be an environmental problem, please check the following points:

    - Try to close other plugins and reinstall and try again
    - Clear browser cache

- If you modify some configurations that do not take effect, please try to click Restore default configuration on the configuration page.
- If a certain website does not take effect or affects the normal display of the website, please send it to the author's mailbox [cangshi520@qq.com](mailto:cangshi520@qq.com) in the following format.

    The specific format is as follows:

    ```
    Web Link：（）
    Details: （）
    Description：（）
    ```

    **This format must be followed, otherwise the feedback will be ignored!**

    Before the version is repaired, you can add rules to exclude the plug-in from taking effect on the specified website: [How to configure the Tampermonkey plug-in to enable the plug-in to take effect only on specific web pages？](https://palerock.cn/articles/001w1s6gHGV?lang=en)


## Version History
- [2022-05-25 - [2.0.12]](https://palerock.cn/node-service/scripts/install/@hook-js_timer/hook.timer.user.js)
  Fix the configuration page failure caused by tampermonkey version update
- [2021-06-15 - [2.0.11]](https://palerock.cn/node-service/scripts/install/@hook-js_timer/hook.timer.user.js)
  Fixed some issues occurred page broken in some web site
- [2021-05-28 - [2.0.10]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.10/hook.timer.user.js)
  1. Fixed issue about native method
  2. Fixed UI Display in baidu.com
- [2021-05-23 - [2.0.9]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.9/hook.timer.user.js)
  Add the adaptation to the self defined sub module, no functional changes
- [2021-05-14 - [2.0.8]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.8/hook.timer.user.js)
  1. Add two configure items for UI
  2. Fix about rate in configuration
  3. Update the UI
- [2021-05-13 - [2.0.7]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.7/hook.timer.user.js)
  1. Update configuration pages UI
  2. Optimize shortcut key
  3. UI optimization => Now the floating ball will be easier to see
- [2021-05-08 - [2.0.6]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.6/hook.timer.user.js) Fix the anomaly that affects the normal operation of some websites after the 2.x refactoring
- [2021-05-07 - [2.0.5]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.5/hook.timer.user.js) Configurable script (custom magnification, custom shortcut keys, custom floating ball position, etc.), fix some other minor issues
- [2021-05-05 - [2.0.4]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.4/hook.timer.user.js) Fix the problem of 1x lock, change the update link and install link

## Version Legacy
The script from 2.0.0 version outset not to rely on GreasyFork, and 1.x version can GreasyFork properly use and install (function is no longer updated, but normal maintenance)
> **We recommend upgrading to 2.0.0 or higher, more stable function, novel**
