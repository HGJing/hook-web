# 计时器掌控者
![Views](https://palerock.cn/node-service/images/greasyfork/views-info/@hook-js_timer_hook.timer.user)
![Daily Installs](https://palerock.cn/node-service/images/scripts/install/@hook-js_timer_hook.timer.user?label=%E4%BB%8A%E6%97%A5%E5%AE%89%E8%A3%85%E6%95%B0&daily=1)
![Daily Updates](https://palerock.cn/node-service/images/scripts/update/@hook-js_timer_hook.timer.user?label=%E4%BB%8A%E6%97%A5%E6%9B%B4%E6%96%B0%E6%95%B0&daily=1)
![Total Installs](https://palerock.cn/node-service/images/scripts/install/@hook-js_timer_hook.timer.user?label=%E6%80%BB%E5%AE%89%E8%A3%85%E6%95%B0)
![得分](https://palerock.cn/node-service/images/greasyfork/info/fan_score/372673?name=得分&rcolor=orange)
![好评](https://palerock.cn/node-service/images/greasyfork/info/good_ratings/372673?name=好评&rcolor=darkcyan)
[![最新版本](https://palerock.cn/node-service/images/badge.svg?name=%E6%9C%80%E6%96%B0%E7%89%88%E6%9C%AC&status=2.0.12&rcolor=%235E70CC)](https://palerock.cn/node-service/scripts/install/@hook-js_timer/hook.timer.user.js)

**计时器掌控者**是一款基于 *Tampermonkey*，用于任意网页的计时器的加/减速、视频播放器的快/慢放的脚本。

> **全新版本上线，在本页安装最新版本的脚本**  
> 2.x 版本新特性：脚本可配置化（自定义倍率，自定义快捷键，自定义悬浮球位置等）  
> 欢迎加入 Q群 讨论：140783474

## 安装/使用方式

1. 任意浏览器安装与之匹配的 *Tampermonkey* 或者 *Violentmonkey* （不推荐使用 *Greasemonkey*）扩展用于管理脚本（[怎样安装？](https://palerock.cn/articles/001rBUq3OLw)）
2. 点击 [安装链接](https://palerock.cn/node-service/scripts/install/@hook-js_timer/hook.timer.user.js) 安装该脚本
3. 安装完成后进入任意网站，网页左边有悬浮的能量球，鼠标移动过去有加速选项，你可以选择加速或减速，最上面的悬浮球表明现在的倍速
4. 快捷键操作：
    - `’ctrl‘+‘=’` 或 `’ctrl‘+'>'` : 增加倍率（默认 +2）
    - `’alt‘+‘=’` 或 `’alt‘+‘>’` : 倍率以倍数增加（默认乘以2）
    - `’ctrl‘+‘-’` 或 `’ctrl‘+‘<’` : 减少倍率（默认 -2）
    - `’alt‘+‘-’` 或 `’ctrl‘+‘<’` : 倍率以倍数减小（默认除以2）
    - `’ctrl‘+‘0’` 或 `’alt‘+‘0’` : 倍率恢复为1
    - `’ctrl‘+‘9’` 或 `’alt‘+‘9’` : 弹出自定义倍率输入框
5. **脚本支持自定义设置了，鼠标右键菜单中选择 `Tampermonkey > 计时器掌控者 > 打开配置页面`，或者直接打开配置链接: [https://timer.palerock.cn/configuration/](https://timer.palerock.cn/configuration/)**

## 用途
关于该脚本的用途，该脚本作为控制网页计时器的存在，可根据以下几点用途来使用该脚本
1. 跳过烦人的广告倒计时
   
     很多视频网站在播放视频前都会有广告，观看广告往往需要至少一两分钟的时间，通过该插件便可以对这个广告倒计时进行加速，甚至可以做到一秒跳过广告。

2. 突破视频播放速度的上/下限，解锁播放速率限制
   
     也是针对于很多视频网站，默认的加速倍数处于 `0.5 - 2.0` 之间也就是慢放一倍或者加速一倍，甚至有的视频网站还不允许改变播放速率，这时该脚本便可以用于解锁播放速率限制，从 `0.065` 到 `16` 倍之间随意切换，增加视频网站的播放效率。

3. 去除无用的等待时间
   
     和视频倒计时广告类似，很多网站会在某些地方设置类似于会员门槛的等待时间，如果不是会员需要等待一段时间，这时便可以用本插件加速等待时间。

4. 更多等待探索的用途
   
     由于本脚本是于根本上作用于浏览器的计时函数，所以凡是需要改变计时效率的地方都可以使用本脚本，欢迎大家一一探索并分享。


## 注意事项
1. 关于于视频的快/慢放
   
   - 仅支持 `HTML5` 视频播放器，不支持 `flash` 播放器（`flash` 播放器已被主流淘汰）
   - 一旦使用该脚本对视频进行快/慢放，视频本身或者其它脚本的播放速度皆会失效，若想使用其它的脚本控制视频速度，请将该脚本的速度设置为默认值：`1`
  
2. 关于有的网站在开启加速后网页显示时间跳过，但刷新网页后倒计时又重置的问题
   
   - 有可能是该网站后台参与了时间运算，该脚本只能作用于浏览器所以无效
   - 有可能是在本地做倒计时，但使用的是未知的倒计时技术，该情况可以反馈并联系作者

3. 该脚本默认对所有网页生效，如果想只对某些网站生效或者让某些排除某些网站，详见以下文章：

    [油猴插件怎么配置能让插件只在特定的网页生效？](https://palerock.cn/articles/001w1s6gHGV)

## 脚本模块化
从 `2.0.0` 版本起，该脚本从底层进行了大幅度的重构，将各个功能模块化，目前有以下几大内置模块
1. `ConfigModule` (核心配置模块，用于实现全局配置统一)
2. `DefinitionModule` (核心定义模块，用于实现底层 Hook)
3. `ShadowDomModule` (核心模块，用于检索 `ShadowDOM`)
4. `TimerModule` (预装模块，用于劫持浏览器计时器函数)
5. `DateModule` (预装模块，用于劫持 `Date` 函数)
6. `VideoSpeedModule` (预装模块，用于劫持 H5 视频播放速率)
7. `ShortcutModule` (预装模块，用于设定快捷键，可扩展)
8. `UiModule` (预装模块，用于基础 UI 显示，可替换)
   
### 进阶模块
该脚本后期会不间断提供各种新的功能模块，比如新的 UI 模块，针对于某些网站生效的特定模块，这些模块将开放于脚本商店，可付费购买

### 自定义模块
联系作者，可有偿获得其它自定义模块

## 关于反馈
- 欢迎各种反馈或建议
- 如果是插件在任何网站都不生效，那肯定是环境问题，请检查以下几点
    - 尝试关闭其它插件并重新安装后再试
    - 清理浏览器缓存
- 如果在修改某些配置后不生效，请尝试在配置页面点击恢复默认配置
- 如果是某个网站不能生效或者影响该网站的正常显示，请以以下格式发送到作者邮箱 [cangshi520@qq.com](mailto:cangshi520@qq.com)
    具体格式如下：

    ```
    网站链接：（必须给出具体页面的链接，不能是一个大的网页，如果需要登录才能看到，需要私信用户名以及密码）
    反馈原因: （计时器不生效/影响网页正常使用/建议）
    描述：（描述具体情况，以方便找到问题并修复）
    ```

    **必须遵循该格式，否则反馈一律无视！**

    在版本修复前可以自己添加规则排除该插件对指定网站生效: [油猴插件怎么配置能让插件只在特定的网页生效？](https://palerock.cn/articles/001w1s6gHGV)

- 加入 QQ 群进行反馈

![image.png](https://palerock.cn/api-provider/files/view?identity=L2FydGljbGUvaW1hZ2UvMjAyMTA1MDgxMjE1MTY2OTVXQW5OODBNRi5wbmc=&w=400)

## 更新历史
- [2022-05-25 - [2.0.12]](https://palerock.cn/node-service/scripts/install/@hook-js_timer/hook.timer.user.js)
  修复因油猴版本更新导致的配置页面失效问题
- [2021-06-15 - [2.0.11]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.11/hook.timer.user.js)
  修复一些网站不能正常运行的问题
- [2021-05-28 - [2.0.10]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.10/hook.timer.user.js)
  1. 修复因使用的原生 JS 方法被网页修改导致脚本出错的问题（Array.prototype.reduce）
  2. 修复在百度搜索页面悬浮球的样式加载失败的问题
- [2021-05-23 - [2.0.9]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.9/hook.timer.user.js)
  增加对自定义子模块的适应，无功能性上的改动
- [2021-05-14 - [2.0.8]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.8/hook.timer.user.js)
  1. 增加两个 UI 上的配置
  2. 修复关于基础倍率的配置异常
  3. 优化加深后的悬浮球样式
- [2021-05-13 - [2.0.7]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.7/hook.timer.user.js)
  1. 更新配置页面，优化配置选项
  2. 减少快捷键冲突（目前该脚本的快捷键优先级会比较高，并且会阻止其它冲突快捷键生效）
  3. UI 优化 => 现在悬浮小球会更容易看到
- [2021-05-08 - [2.0.6]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.6/hook.timer.user.js) 修复因2.x重构后影响某些网站正常运行的异常
- [2021-05-07 - [2.0.5]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.5/hook.timer.user.js) 脚本可配置化（自定义倍率，自定义快捷键，自定义悬浮球位置等）, 修复一些其它的小问题
- [2021-05-05 - [2.0.4]](https://palerock.cn/node-service/scripts/install/@hook-js_timer@2.0.4/hook.timer.user.js) 修复1倍锁定的问题，更改更新链接和安装链接
- 2021-05-04 - [2.0.3] 功能完善，修复一些小 bug
- 2021-05-04 - [2.0.0] 全新脚本上线，功能模块化，新模块陆续开放中

## 版本遗留
该脚本从 `2.0.0` 版本开始便不依托于 `GreasyFork`，而 `1.x` 版本能够在 `GreasyFork` 上正常使用并安装（功能不再更新，但正常维护）
> **建议升级为 `2.0.0` 以上版本，功能更加稳定、新颖**

## 喜欢该脚本

![image.png](https://palerock.cn/api-provider/files/view?identity=L2FydGljbGUvaW1hZ2UvMjAyNDA2MjUxMTU4MTMyNjE3TlBobGsxWC5wbmc=&w=400)
